#!/usr/bin/env bash
set -euo pipefail

WHITELIST=$(cat <<EOF
COPYING
LICENSE
config.json.sample
EOF
)

FILES_WITHOUT_SPDX=$(git grep --files-without-match SPDX | grep -v -E "(\.json|\.patch|\.md|\.lock)$")

BAD_FILES=$(comm -23 \
  <(echo "$FILES_WITHOUT_SPDX" | sort | uniq) \
  <(echo "$WHITELIST" | sort | uniq))

[[ -z "$BAD_FILES" ]] || {
  echo "$BAD_FILES"
  exit 1
}
