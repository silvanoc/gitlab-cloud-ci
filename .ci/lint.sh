#!/usr/bin/env sh
# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

set -u

flake8 \
  --max-line-length=88 \
  siemens tests
