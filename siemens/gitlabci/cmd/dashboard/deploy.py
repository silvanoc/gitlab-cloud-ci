# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os

from siemens.gitlabci.cli import dashboard as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    from sh import helm, kubectl

log = logging.getLogger(__name__)


@cli.command()
def deploy():
    """Deploy Kubernetes dashboard"""
    log.info("Deploying Kubernetes dashboard")

    try:
        kubectl("create", "-f", "./share/k8s/dashboard/dashboard-admin.yaml")
    except Exception:
        log.warning("Error creating admin user")

    helm("repo", "add", "dashboard", "https://kubernetes.github.io/dashboard/")
    helm("repo", "update")
    helm(
        "install",
        "dashboard/kubernetes-dashboard",
        "--name-template",
        "kubernetes-dashboard",
        "--namespace",
        "kube-system",
        "-f",
        "./share/k8s/dashboard/values.yaml",
    )
    log.info("Success! Your K8s cluster is up and running.")
