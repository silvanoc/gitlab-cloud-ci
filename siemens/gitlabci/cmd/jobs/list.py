# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os

import click

from siemens.gitlabci.cli import jobs as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    from sh import kubectl


@cli.command()
@click.option(
    "--namespace", default="gitlab", help="Use the given namespace", show_default=True
)
def list(namespace):
    """List currently running CI jobs."""
    print(_list(namespace))


def _list(namespace):
    return kubectl("get", "pods", "--selector=!app", "--namespace={}".format(namespace))
