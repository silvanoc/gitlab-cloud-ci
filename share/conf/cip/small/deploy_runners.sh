#!/usr/bin/env bash
###############################################################################
# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
###############################################################################
set -euo pipefail
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"
source "$SCRIPT_DIR/../lib.sh"

EPOCH=$(date "+%s")
ask_confirm gitlabci runner deploy "--token=$TOKEN_CIP_PLAYGROUND" "--config=$SCRIPT_DIR/runner.yaml" "$CLUSTER_NAME-playground-$EPOCH"
ask_confirm gitlabci runner deploy "--token=$TOKEN_CIP_PROJECT" "--config=$SCRIPT_DIR/runner.yaml" "$CLUSTER_NAME-project-$EPOCH"
# ask_confirm gitlabci runner deploy "--token=$TOKEN_CIP_GITLAB_CLOUD_CI=" "--config=$SCRIPT_DIR/runner.yaml" "gitlab-cloud-ci-testing-$EPOCH"
