#!/usr/bin/env bash
###############################################################################
# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
###############################################################################
set -euo pipefail
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"
source "$SCRIPT_DIR/lib.sh"

echo "Creating cluster $CLUSTER_NAME."
ask_confirm gitlabci create aws
ask_confirm gitlabci hardening deploy
ask_confirm gitlabci autoscaler deploy
ask_confirm gitlabci monitoring deploy
ask_confirm gitlabci dashboard deploy

ask_confirm "$SCRIPT_DIR"/deploy_runners.sh
